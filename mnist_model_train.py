from keras.models import load_model
import keras.utils.np_utils as kutils
import pandas as pd


class MnistRecognizer(object):
    def __init__(self, model_file):
        self.model = load_model(model_file)
        self.model.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=["accuracy"])

    def train(self, mnist_file, num_steps, num_epochs, data_batch_size, train_batch_size, save_model_file=None):
        losses, accs = [], []
        for i in range(num_steps):
            data = MnistData(mnist_file, data_batch_size)
            while True:
                try:
                    batchX, batchY = next(data)
                    history = self.model.fit(batchX, batchY,
                                             batch_size=train_batch_size,
                                             epochs=num_epochs,
                                             verbose=True)
                    losses.append(history.history['loss'])
                    accs.append(history.history['acc'])
                except StopIteration:
                    break
            if save_model_file is not None:
                self.model.save(save_model_file)


class MnistData(object):
    def __init__(self, mnist_file, batch_size, start_i=0, stop_i=None):
        train = pd.read_csv(mnist_file).values
        img_rows, img_cols = 28, 28
        self.trainX = train[:, 1:].reshape(train.shape[0], img_rows, img_cols, 1)
        self.trainX.astype(float)
        self.trainX = self.trainX / 255.0
        self.trainY = train[:, 0]
        self.trainY = kutils.to_categorical(train[:, 0])
        self.stop_i = train.shape[0] if stop_i is None else stop_i
        self.i = start_i
        self.batch_size = batch_size

    def __iter__(self):
        return self

    def __next__(self):
        if self.i > self.stop_i - 1:
            raise StopIteration
        else:
            batchX = self.trainX[self.i:self.i+self.batch_size]
            batchY = self.trainY[self.i:self.i+self.batch_size]
            print('batchX shape {}, batchY shape {}'.format(batchX.shape, batchY.shape))
            self.i += self.batch_size
            return batchX, batchY


if __name__ == '__main__':
    mnist_rec = MnistRecognizer('mnist_model.h5')
    mnist_rec.train('train.csv', 1, 1, 128, 16, 'mnist_model.h5')
